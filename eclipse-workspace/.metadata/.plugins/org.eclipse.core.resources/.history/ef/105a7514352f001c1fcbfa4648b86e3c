module SimplePDL2PetriNet;
create OUT: petrinet from  IN: simplepdl;

-- Obtenir le processus qui contient ce process element.
-- Remarque: Ce helper ne serait pas utile si une référence opposite
-- avait été placée entre Process et ProcessElement
helper context simplepdl!ProcessElement
def: getProcess(): simplepdl!Process =
	simplepdl!Process.allInstances()
		->select(p | p.processElements->includes(self))
		->asSequence()->first();

-- Traduire un Process en un PetriNet de même nom
rule Process2PetriNet {
	from p: simplepdl!Process
	to pn: petrinet!PetriNet (name <- p.name)
}

-- Traduire une WorkDefinition en un motif sur le réseau de Petri
rule WorkDefinition2PetriNet {
	from wd: simplepdl!WorkDefinition
	to
		-- PLACES d'une WorkDefinition
		p_idle: petrinet!Place(
				name <- wd.name + '_idle',
				tokens <- 1,
				net <- wd.process),
		p_running: petrinet!Place(
				name <- wd.name + '_running',
				tokens <- 0,
				net <- wd.process),
		p_started: petrinet!Place(
				name <- wd.name + '_started',
				tokens <- 0,
				net <- wd.process),
		p_finished: petrinet!Place(
				name <- wd.name + '_finished',
				tokens <- 0,
				net <- wd.process),
		-- TRANSITIONS d'une WorkDefinition
		t_start: petrinet!Transition(
				name <- wd.name + '_start'),
		t_finish: petrinet!Transition(
				name <- wd.name + '_finish'),
		-- ARCS d'un WorkDefinition
		a_idle2start: petrinet!Arc(
				place <- p_idle,
				transition <- t_start,
				outgoing <- false,
				weight <- 1),
		a_start2running: petrinet!Arc(
				place <- p_running,
				transition <- t_start,
				outgoing <- true,
				weight <- 1),
		a_start2started: petrinet!Arc(
				place <- p_started,
				transition <- t_start,
				outgoing <- true,
				weight <- 1),
		a_running2finish: petrinet!Arc(
				place <- p_running,
				transition <- t_finish,
				outgoing <- false,
				weight <- 1),
		a_finish2finished: petrinet!Arc(
				place <- p_finished,
				transition <- t_finish,
				outgoing <- true,
				weight <- 1)
				
}