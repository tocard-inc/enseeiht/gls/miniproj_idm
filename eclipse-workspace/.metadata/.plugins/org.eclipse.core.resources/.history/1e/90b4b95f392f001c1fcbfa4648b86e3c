module SimplePDL2PetriNet;
create OUT: petrinet from  IN: simplepdl;

-- Obtenir le processus qui contient ce process element.
-- Remarque: Ce helper ne serait pas utile si une référence opposite
-- avait été placée entre Process et ProcessElement
helper context simplepdl!ProcessElement
def: getProcess(): simplepdl!Process =
	simplepdl!Process.allInstances()
		->select(p | p.processElements->includes(self))
		->asSequence()->first();

-- Obtenir la place correspondant au predecesseur d'une WorkSequence
helper context simplepdl!WorkSequence
def: getPlaceOfPredecessor(): petrinet!Place =
	if self.linkType = #startToStart or self.linkType = #startToFinish then
		petrinet!Place.allInstances()
			->select(p | p.name = self.predecessor.name + '_started')
			->asSequence()->first()
	else
		petrinet!Place.allInstances()
			->select(p | p.name = self.predecessor.name + '_finished')
			->asSequence()->first()
	endif;

-- Obtenir la transition correspondant au successeur d'une WorkSequence
helper context simplepdl!WorkSequence
def: getTransitionOfSuccessor(): petrinet!Transition =
	if self.linkType = #startToStart or self.linkType = #finishToStart then
		petrinet!Transition.allInstances()
			->select(t | t.name = self.successor.name + '_start')
			->asSequence()->first()
	else
		petrinet!Transition.allInstances()
			->select(t | t.name = self.successor.name + '_finish')
			->asSequence()->first()
	endif;

-- Obtenir la place correspondant a la Resource d'une Request
helper context simplepdl!Request
def: getPlaceOfResource(): petrinet!Transition =
	petrinet!Transition.allInstances()
		->select(t | t.name = self.successor.name + '_start')
		->asSequence()->first();

-- Traduire un Process en un PetriNet de même nom
rule Process2PetriNet {
	from p: simplepdl!Process
	to pn: petrinet!Network (name <- p.name)
}

-- Traduire une WorkDefinition en un motif sur le réseau de Petri
rule WorkDefinition2PetriNet {
	from wd: simplepdl!WorkDefinition
	to
		-- PLACES d'une WorkDefinition
		p_idle: petrinet!Place(
				name <- wd.name + '_idle',
				tokens <- 1,
				network <- wd.process),
		p_running: petrinet!Place(
				name <- wd.name + '_running',
				tokens <- 0,
				network <- wd.process),
		p_started: petrinet!Place(
				name <- wd.name + '_started',
				tokens <- 0,
				network <- wd.process),
		p_finished: petrinet!Place(
				name <- wd.name + '_finished',
				tokens <- 0,
				network <- wd.process),
		-- TRANSITIONS d'une WorkDefinition
		t_start: petrinet!Transition(
				name <- wd.name + '_start',
				network <- wd.process),
		t_finish: petrinet!Transition(
				name <- wd.name + '_finish',
				network <- wd.process),
		-- ARCS d'un WorkDefinition
		a_idle2start: petrinet!Arc(
				place <- p_idle,
				transition <- t_start,
				outgoing <- false,
				weight <- 1),
		a_start2running: petrinet!Arc(
				place <- p_running,
				transition <- t_start,
				outgoing <- true,
				weight <- 1),
		a_start2started: petrinet!Arc(
				place <- p_started,
				transition <- t_start,
				outgoing <- true,
				weight <- 1),
		a_running2finish: petrinet!Arc(
				place <- p_running,
				transition <- t_finish,
				outgoing <- false,
				weight <- 1),
		a_finish2finished: petrinet!Arc(
				place <- p_finished,
				transition <- t_finish,
				outgoing <- true,
				weight <- 1)	
}

-- Traduire une WorkDefinition en un motif sur le réseau de Petri
rule WorkSequence2PetriNet {
	from ws: simplepdl!WorkSequence
	to
		-- ARCS d'une WorkSequence
		arc1: petrinet!Arc(
				place <- ws.getPlaceOfPredecessor(),
				transition <- ws.getTransitionOfSuccessor(),
				outgoing <- false,
				weight <- 1),
		arc2: petrinet!Arc(
				place <- ws.getPlaceOfPredecessor(),
				transition <- ws.getTransitionOfSuccessor(),
				outgoing <- true,
				weight <- 1)
}

-- Traduire une Resource en un motif sur le réseau de Petri
rule Resource2PetriNet {
	from res: simplepdl!Resource
	to
		-- PLACE d'une Resource
		place: petrinet!Place(
				name <- res.name + '_resource',
				tokens <- res.quantity,
				network <- res.process)
}

-- Traduire une Request en un motif sur le réseau de Petri
rule Request2PetriNet {
	from req: simplepdl!Request
	to
		-- ARCS d'une Request
		arcs1 petrinet!Arc(
				place <- req.getPlaceOfPredecessor(),
				transition <- req.getTransitionOfSuccessor(),
				outgoing <- true,
				weight <- req.quantity)
}