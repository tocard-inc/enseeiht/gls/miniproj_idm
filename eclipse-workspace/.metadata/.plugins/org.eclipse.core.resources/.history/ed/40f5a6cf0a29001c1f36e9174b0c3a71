import 'simplepdl.ecore'

package simplepdl

context Process
inv validName('Invalid name: ' + self.name):
	self.name.matches('[A-Za-z_][A-Za-z0-9_]*')

context ProcessElement
def: process(): Process =
	Process.allInstances()
		->select(p | p.processElements->includes(self))
		->asSequence()->first()

context WorkSequence
inv successorAndPredecessorInSameProcess('Activities not in the same process : '
		+ self.predecessor.name + ' in ' + self.predecessor.process().name+ ' and '
		+ self.successor.name + ' in ' + self.successor.process().name):
	self.process() = self.successor.process()
	and self.process() = self.predecessor.process()

context Process
inv uniqNames: self.processElements
	->select(pe | pe.oclIsKindOf(WorkDefinition))
	->collect(pe | pe.oclAsType(WorkDefinition))
	->forAll(w1, w2 | w1 = w2 or w1.name <> w2.name)

context WorkSequence
inv notReflexive: self.predecessor <> self.successor

context Process
inv nameMin2Char: self.processElements
	->select(pe | pe.oclIsKindOf(WorkDefinition))
	->collect(pe | pe.oclAsType(WorkDefinition))
	->forAll(w | w.name.matches('..+'))

context Process
inv weirdName: self.processElements
	->select(pe | pe.oclIsKindOf(WorkDefinition))
	->collect(pe | pe.oclAsType(WorkDefinition))
	->forAll(w | not w.name.matches('([0-9]*|[a-zA-Z]*|_*)'))

endpackage