import 'simplePDL.ecore'

package simplepdl

context Process
inv validName('Invalid name: ' + self.name):
	self.name.matches('[A-Za-z_][A-Za-z0-9_]*')
inv uniqNamesWD: self.processElements
	->select(pe | pe.oclIsKindOf(WorkDefinition))
	->collect(pe | pe.oclAsType(WorkDefinition))
	->forAll(w1, w2 | w1 = w2 or w1.name <> w2.name)
inv uniqNamesRes: self.processElements
	->select(pe | pe.oclIsKindOf(Resource))
	->collect(pe | pe.oclAsType(Resource))
	->forAll(r1, r2 | r1 = r2 or r1.name <> r2.name)
	
context ProcessElement
def: process(): Process =
	Process.allInstances()
		->select(p | p.processElements->includes(self))
		->asSequence()->first()

context WorkSequence
inv successorAndPredecessorInSameProcess('Activities not in the same process : '
		+ self.predecessor.name + ' in ' + self.predecessor.process().name+ ' and '
		+ self.successor.name + ' in ' + self.successor.process().name):
	self.process() = self.successor.process()
	and self.process() = self.predecessor.process()
inv notReflexive: self.predecessor <> self.successor

context WorkDefinition
inv nameMin2Char: self.name.matches('..+')
inv weirdName: not self.name.matches('([0-9]*|[a-zA-Z]*|_*)')

context Resource
inv negativeQuantity: self.quantity > 0
inv nameMin2Char: self.name.matches('..+')
inv weirdName: not self.name.matches('([0-9]*|_*)')

context Request
inv negativeQuantity: self.quantity > 0
inv greedy: self.quantity <= self.target.quantity

endpackage