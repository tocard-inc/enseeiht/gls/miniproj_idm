package simplepdl.manip;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import petrinet.PetrinetPackage;
import petrinet.Place;
import petrinet.Transition;
import petrinet.Arc;
import petrinet.Network;
import petrinet.PetrinetFactory;
import simplepdl.Process;
import simplepdl.WorkDefinition;
import simplepdl.WorkSequence;
import simplepdl.WorkSequenceType;
import simplepdl.SimplepdlFactory;
import simplepdl.SimplepdlPackage;

public class simplepdl2petrinet {

	public static void main(String[] args) {
		
		// Charger les package SimplePDL et Petrinet afin de les enregistrer dans le registre d'Eclipse.
		SimplepdlPackage packageInstance = SimplepdlPackage.eINSTANCE;
		PetrinetPackage packageInstance2 = PetrinetPackage.eINSTANCE;
		
		// Enregistrer l'extension ".xmi" comme devant être ouverte à
		// l'aide d'un objet "XMIResourceFactoryImpl"
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("xmi", new XMIResourceFactoryImpl());
		
		// Créer un objet resourceSetImpl qui contiendra une ressource EMF (le modèle)
		ResourceSet resSet = new ResourceSetImpl();

		// Charger la ressource (notre modèle)
		URI modelURI = URI.createURI("pdl-sujet.xmi");
		Resource resource = resSet.getResource(modelURI, true);
			
		// Récupérer le premier élément du modèle (élément racine)
		Process process = (Process) resource.getContents().get(0);
		
		// La fabrique pour fabriquer les éléments de PetriNET
	    PetrinetFactory myFactory = PetrinetFactory.eINSTANCE;
	    
		// Créer un élément Network
		Network network = myFactory.createNetwork();
		network.setName(process.getName());
		
		// Conversion des WorkDefinition en Node et Transition
		for (Object o : process.getProcessElements()) {
			if (o instanceof WorkDefinition) {
				WorkDefinition wd = (WorkDefinition) o;
				String name = wd.getName();
				System.out.println("  - " + name);
				
				Place idle = myFactory.createPlace();
				idle.setName(name + "_idle");
				idle.setTokens(1);
				Place running = myFactory.createPlace();
				running.setName(name + "_running");
				running.setTokens(1);
				Place finished = myFactory.createPlace();
				finished.setName(name + "_finished");
				finished.setTokens(1);
				
				Arc pause2start = myFactory.createArc();
				pause2start.setTarget(idle);
				pause2start.setOutgoing(false);
				pause2start.setWeight(1);
				Arc start2running = myFactory.createArc();
				start2running.setTarget(running);
				start2running.setOutgoing(true);
				start2running.setWeight(1);
				Transition start = myFactory.createTransition();
				start.setName(name + "_start");
				start.getArcs().add(pause2start);
				start.getArcs().add(start2running);
				
				Arc running2finish = myFactory.createArc();
				running2finish.setTarget(running);
				running2finish.setOutgoing(false);
				running2finish.setWeight(1);
				Arc finish2finished = myFactory.createArc();
				finish2finished.setTarget(finished);
				finish2finished.setOutgoing(true);
				finish2finished.setWeight(1);
				Transition finish = myFactory.createTransition();
				finish.setName(name + "_finish");
				finish.getArcs().add(running2finish);
				finish.getArcs().add(finish2finished);
				
				System.out.println("    + " + idle.getName() + " (" + idle.getTokens() + ")");
				
				network.getNodes().add(idle);
				network.getNodes().add(start);
				network.getNodes().add(running);
				network.getNodes().add(finish);
				network.getNodes().add(finished);
			}
		}
		
		// Créer le nouveau xmi (modèle convertit)
		URI convURI = URI.createURI("test.xmi");
		Resource conv = resSet.createResource(convURI);
		
		// Sauver la ressource
	    try {
	    	conv.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
