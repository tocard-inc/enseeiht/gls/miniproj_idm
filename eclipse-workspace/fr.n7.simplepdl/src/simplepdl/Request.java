/**
 */
package simplepdl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link simplepdl.Request#getQuantity <em>Quantity</em>}</li>
 *   <li>{@link simplepdl.Request#getTarget <em>Target</em>}</li>
 *   <li>{@link simplepdl.Request#getRequester <em>Requester</em>}</li>
 * </ul>
 *
 * @see simplepdl.SimplepdlPackage#getRequest()
 * @model
 * @generated
 */
public interface Request extends EObject {
	/**
	 * Returns the value of the '<em><b>Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity</em>' attribute.
	 * @see #setQuantity(int)
	 * @see simplepdl.SimplepdlPackage#getRequest_Quantity()
	 * @model required="true"
	 * @generated
	 */
	int getQuantity();

	/**
	 * Sets the value of the '{@link simplepdl.Request#getQuantity <em>Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity</em>' attribute.
	 * @see #getQuantity()
	 * @generated
	 */
	void setQuantity(int value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link simplepdl.Resource#getRequests <em>Requests</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Resource)
	 * @see simplepdl.SimplepdlPackage#getRequest_Target()
	 * @see simplepdl.Resource#getRequests
	 * @model opposite="requests" required="true"
	 * @generated
	 */
	Resource getTarget();

	/**
	 * Sets the value of the '{@link simplepdl.Request#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Resource value);

	/**
	 * Returns the value of the '<em><b>Requester</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link simplepdl.WorkDefinition#getRequests <em>Requests</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requester</em>' container reference.
	 * @see #setRequester(WorkDefinition)
	 * @see simplepdl.SimplepdlPackage#getRequest_Requester()
	 * @see simplepdl.WorkDefinition#getRequests
	 * @model opposite="requests" required="true" transient="false"
	 * @generated
	 */
	WorkDefinition getRequester();

	/**
	 * Sets the value of the '{@link simplepdl.Request#getRequester <em>Requester</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requester</em>' container reference.
	 * @see #getRequester()
	 * @generated
	 */
	void setRequester(WorkDefinition value);

} // Request
