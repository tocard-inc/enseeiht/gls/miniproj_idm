/**
 */
package simplepdl.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import simplepdl.Guidance;
import simplepdl.ProcessElement;
import simplepdl.SimplepdlPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link simplepdl.impl.ProcessElementImpl#getProcess <em>Process</em>}</li>
 *   <li>{@link simplepdl.impl.ProcessElementImpl#getGuidances <em>Guidances</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ProcessElementImpl extends MinimalEObjectImpl.Container implements ProcessElement {
	/**
	 * The cached value of the '{@link #getGuidances() <em>Guidances</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuidances()
	 * @generated
	 * @ordered
	 */
	protected EList<Guidance> guidances;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProcessElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SimplepdlPackage.Literals.PROCESS_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public simplepdl.Process getProcess() {
		if (eContainerFeatureID() != SimplepdlPackage.PROCESS_ELEMENT__PROCESS) return null;
		return (simplepdl.Process)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetProcess(simplepdl.Process newProcess, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newProcess, SimplepdlPackage.PROCESS_ELEMENT__PROCESS, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcess(simplepdl.Process newProcess) {
		if (newProcess != eInternalContainer() || (eContainerFeatureID() != SimplepdlPackage.PROCESS_ELEMENT__PROCESS && newProcess != null)) {
			if (EcoreUtil.isAncestor(this, newProcess))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newProcess != null)
				msgs = ((InternalEObject)newProcess).eInverseAdd(this, SimplepdlPackage.PROCESS__PROCESS_ELEMENTS, simplepdl.Process.class, msgs);
			msgs = basicSetProcess(newProcess, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SimplepdlPackage.PROCESS_ELEMENT__PROCESS, newProcess, newProcess));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Guidance> getGuidances() {
		if (guidances == null) {
			guidances = new EObjectWithInverseResolvingEList.ManyInverse<Guidance>(Guidance.class, this, SimplepdlPackage.PROCESS_ELEMENT__GUIDANCES, SimplepdlPackage.GUIDANCE__ELEMENTS);
		}
		return guidances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SimplepdlPackage.PROCESS_ELEMENT__PROCESS:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetProcess((simplepdl.Process)otherEnd, msgs);
			case SimplepdlPackage.PROCESS_ELEMENT__GUIDANCES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getGuidances()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SimplepdlPackage.PROCESS_ELEMENT__PROCESS:
				return basicSetProcess(null, msgs);
			case SimplepdlPackage.PROCESS_ELEMENT__GUIDANCES:
				return ((InternalEList<?>)getGuidances()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case SimplepdlPackage.PROCESS_ELEMENT__PROCESS:
				return eInternalContainer().eInverseRemove(this, SimplepdlPackage.PROCESS__PROCESS_ELEMENTS, simplepdl.Process.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SimplepdlPackage.PROCESS_ELEMENT__PROCESS:
				return getProcess();
			case SimplepdlPackage.PROCESS_ELEMENT__GUIDANCES:
				return getGuidances();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SimplepdlPackage.PROCESS_ELEMENT__PROCESS:
				setProcess((simplepdl.Process)newValue);
				return;
			case SimplepdlPackage.PROCESS_ELEMENT__GUIDANCES:
				getGuidances().clear();
				getGuidances().addAll((Collection<? extends Guidance>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SimplepdlPackage.PROCESS_ELEMENT__PROCESS:
				setProcess((simplepdl.Process)null);
				return;
			case SimplepdlPackage.PROCESS_ELEMENT__GUIDANCES:
				getGuidances().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SimplepdlPackage.PROCESS_ELEMENT__PROCESS:
				return getProcess() != null;
			case SimplepdlPackage.PROCESS_ELEMENT__GUIDANCES:
				return guidances != null && !guidances.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ProcessElementImpl
